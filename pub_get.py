from bs4 import BeautifulSoup
import requests
import pandas as pd
import re
from scholarly import scholarly


def get_search_results_df(url):
    columns = ["rank", "タイトル", "writer", "year", "citations", "url"]
    df = pd.DataFrame(columns=columns)  # 表の作成
    html_doc = requests.get(url).text
    soup = BeautifulSoup(html_doc, "html.parser")  # BeautifulSoupの初期化
    tags1 = soup.find_all("a", attrs={"class": "gsc_a_at"})  # title&url
    tags2 = soup.find_all("div", {"class": "gs_gray"})  # writer&year
    tags3 = soup.find_all("a", attrs={"class":"gsc_a_ac gs_ibl"})  # citation
    rank = 1


    for tag1, tag2, tag3 in zip(tags1, tags2, tags3):
        title = tag1.text.replace("[HTML]", "")
        writer = tag2.text
        writer = re.sub(r'\d', '', writer)
        year = tag2.text
        year = re.sub(r'\D', '', year)
        citations = tag3.text.replace("[HTML]", "")
        se = pd.Series([rank, title, writer, year, citations, url], columns)
        df = df.append(se, columns)
        rank += 1
    return df


url = "https://scholar.google.co.jp/citations?hl=ja&user=PWjp53oAAAAJ&view_op=list_works&sortby=pubdate"

search_results_df = get_search_results_df(url)
filename = "Google_Scholar.csv"
search_results_df.to_csv(filename, encoding="utf-8")

# Retrieve the author's data, fill-in, and print
search_query = scholarly.search_author('Ken Kiyono')
author = next(search_query).fill()
print(author)