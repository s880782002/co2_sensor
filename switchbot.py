import asyncio
import serial
from bleak import BleakClient

address = "FD:A7:D9:40:91:21"  # device bluetooth Mac address
UUID = "CBA20002-224D-11E6-9FB8-0002A5D5C51B"  # press button or not

port = serial.Serial('COM14', 115200)  # port to communicate with esp32

"""
if read_line[0:7] == "AVERAGE":
    _, _, _, humidity = read_line.split(" ")
    print(read_line, humidity)
return humidity
"""

push = b'\x57\x01'
button = [0]

before = 0
humidity = 0


def read_humidity():
    read_line = port.readline()
    read_line = read_line.decode("utf-8")
    print(read_line, type(read_line))
    global before, humidity
    if read_line[0:7] == "AVERAGE":
        _, _, _, humidity = read_line.split(" ")
        humidity = humidity
        print("humidity:", humidity)

    if 18.0 < float(humidity) < 21.0 and before == 1:
        before = 1
    elif 18.0 < float(humidity) < 21.0 and before == 0:
        before = 0
    elif float(humidity) <= 18.0:
        before = 1
    elif 21.0 <= float(humidity):
        before = 0

    return before


async def press_button(address, loop):
    async with BleakClient(address, loop=loop) as client:
        x = await client.is_connected()
        print("Connected: {0}".format(x))
        y = await client.write_gatt_char(UUID, push)
        print(y)


while 1:
    """
        1. reconnect mechanism is needed
        2. 
    """
    loop = asyncio.get_event_loop()
    button.append(read_humidity())
    if abs(button[-1] - button[-2]) != 0:
        print(button)
        loop.run_until_complete(press_button(address, loop))
