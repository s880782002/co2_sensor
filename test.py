import logging
import threading
import time

logging.basicConfig(level=logging.DEBUG, format='%(threadName)s: %(message)s')


def worker1():
    # thread の名前を取得
    logging.debug('start')
    time.sleep(5)   # thread suspending
    logging.debug('end')


def worker2(x, y=1):
    logging.debug('start')
    logging.debug(x)
    logging.debug(y)
    time.sleep(2)   # thread suspending
    logging.debug('end')


if __name__ == '__main__':
    # スレッドに workder1 関数を渡す
    t1 = threading.Thread(name='rename worker1', target=worker1)    # thread name : rename worker1
    t1.setDaemon(True)
    t2 = threading.Thread(target=worker2, args=(100, ), kwargs={'y': 200})  # thread name thread-1
    # スレッドスタート
    t1.start()  # if t2 is finished but t1 is not, t1 will be forced to finish
    t2.start()
    print('started')
    # t1.join()
